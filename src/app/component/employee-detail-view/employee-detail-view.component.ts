import {Component, Input, OnInit} from '@angular/core';
import {EmployeeService} from "../../service/employee.service";
import {Router} from "@angular/router";
import {AppComponent} from "../../app.component";
import {Employee} from "../../model/Employee";
import {Observable} from "rxjs";

@Component({
  selector: 'app-employee-detail-view',
  templateUrl: './employee-detail-view.component.html',
  styleUrls: ['./employee-detail-view.component.css']
})
export class EmployeeDetailViewComponent implements OnInit {

  tempBoi: Observable<Employee> | undefined;
  lastname: string = "";
  firstname: string = "";
  street: string = "";
  postcode: string = "";
  city: string = "";
  phone: string = "";
  @Input()
  id: number = 0;

  constructor(
    private router: Router,
    private employeeService: EmployeeService,
    private app : AppComponent) {
    this.app.header = 1;
  }

  editEmployee(){
    if (this.lastname === null || this.firstname === null ||
      this.street === null || this.postcode === null||
      this.city === null || this.phone === null ||
      this.id === null)
    {
      return;
    }

    if (this.lastname === "" || this.firstname === "" ||
      this.street === "" || this.postcode === "" ||
      this.city === "" || this.phone === "" ||
      this.id === 0)
    {
      return;
    }

    this.employeeService.updateEmployee(
      new Employee(
        this.id,
        this.lastname,
        this.firstname,
        this.street,
        this.postcode,
        this.city,
        this.phone)
    )
    this.router.navigate(['employee']);
  }

  ngOnInit(): void {
    this.tempBoi = this.employeeService.getEmployee(this.id)
  }

  returnToMainView() {
    this.router.navigate(['employee']);
  }

}
