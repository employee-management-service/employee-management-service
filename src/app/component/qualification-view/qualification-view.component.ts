import {Component, Input, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import { Observable } from 'rxjs';
import { QualificationService } from 'src/app/service/qualification.service';
import {Employee} from "../../model/Employee";


@Component({
  selector: 'app-qualification-view',
  templateUrl: './qualification-view.component.html',
  styleUrls: ['./qualification-view.component.css']
})
export class QualificationViewComponent implements  OnInit{

  employees: Observable<Employee[]> | undefined;
  @Input()
  designation: string = "";

  constructor(
    private qualifikationService : QualificationService,
    public router: Router,
  ) {}

  loadEmployees() {
    this.employees = this.qualifikationService.findEmployeesByQualification(this.designation);
  }

  return() {
    this.router.navigateByUrl("employee");
  }

  editEmployee(id: number){
    this.router.navigate(['employee/detail', id]);
  }

  ngOnInit() {
    this.loadEmployees();
  }
}
