import { Injectable } from '@angular/core';
import { User } from '../model/User';
import { Router } from '@angular/router';
import { BearerTokenHolderService } from '../service/BearerTokenHolder';
import { AppComponent } from '../app.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BearerToken } from '../model/BearerToken';
import { count, tap } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
  export class Login
  {
    public readonly user = new User('User', 'User');
    private readonly url = "http://authproxy.szut.dev/";
    badTries = 0;

    constructor
    (
      private router : Router,
      private bearerTokenService : BearerTokenHolderService,
      private app : AppComponent,
      private http : HttpClient
    ) {}

    tryLogin(signInData: User) : boolean
    {
      if (signInData === null)
      {
        //todo login failed aufrufen something went wrong
        return false;
      }

      if (!this.checkUser(signInData.getUser()))
      {
        //todo login fail user wrong
        this.badTries++;
        this.checkIfUserHasToBeLocked()
        return false;
      }

      if (!this.checkPassword(signInData.getPassword()))
      {
        //todo login falsch password wrong
        this.badTries++;
        this.checkIfUserHasToBeLocked()
        return false
      }
      this.getBearerToken(signInData)
      return true;
    }

    private getBearerToken(signInData: User)
    {
      const headers: HttpHeaders = new HttpHeaders(
      ).append("Content-Type", "application/x-www-form-urlencoded");
      const body = `grant_type=password&client_id=employee-management-service&username=${signInData.username}&password=${signInData.password}`;
      this.http.post<BearerToken>(this.url, body.toString(), {headers}).subscribe(data => {
        this.bearerTokenService.bearer = data;
      });
    }

    private checkIfUserHasToBeLocked()
    {
      if (this.badTries === 3) {
        //todo login fialed to many tries
        setTimeout(() => {
          this.badTries = 0;
        }, 300000);
      }
    }

    private checkUser(userName: string): boolean
    {
      return userName === this.user.getUser();
    }

    private checkPassword(password: string): boolean
    {
      return password === this.user.getPassword();
    }
  }
