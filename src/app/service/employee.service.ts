import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject, tap } from 'rxjs';
import { Employee } from '../model/Employee';
import { BearerTokenHolderService } from '../../app/service/BearerTokenHolder';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor (
    private http: HttpClient,
    private bearerService : BearerTokenHolderService,
  ) { }

  public getEmployees() : Observable<Employee[]> {

    return this.http.get<Employee[]>('/backend/employees', {
      headers : new HttpHeaders()
        .append('Authorization', `Bearer ${this.bearerService.bearer.access_token}`)
        .append('Content-Type', `application/json`)
    })
  }

  public getEmployee(id : number) : Observable<Employee> {

    return this.http.get<Employee>('/backend/employees/' + id, {
      headers : new HttpHeaders().append('Authorization', `Bearer ${this.bearerService.bearer.access_token}`).append('Content-Type', `application/json`)
    })
  }

  public postEmployees(employee : Employee) {
    this.http.post<Employee>('/backend/employees', employee, {
      headers : new HttpHeaders()
        .append('Authorization', `Bearer ${this.bearerService.bearer.access_token}`)
        .append('Content-Type', `application/json`)
    });
  }

  public deleteEmployee(id : number) {
    this.http.delete(`/backend/employees/${id}`, {
      headers : new HttpHeaders()
        .append('Authorization', `Bearer ${this.bearerService.bearer.access_token}`)
        .append('Content-Type', `application/json`)
    });
  }

  public updateEmployee(employee : Employee) {
    this.http.put<Employee>(`/backend/employees/${employee.id}`, JSON.stringify(employee),  {
      headers : new HttpHeaders()
        .append('Authorization', `Bearer ${this.bearerService.bearer.access_token}`)
        .append('Content-Type', `application/json`)
    })
  }
}
