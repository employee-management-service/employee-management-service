import { Component} from '@angular/core';
import {Router} from "@angular/router";
import { EmployeeService } from 'src/app/service/employee.service';
import {Employee} from "../../model/Employee";
import {AppComponent} from "../../app.component";
import {isEmpty} from "rxjs";

@Component({
  selector: 'app-employee-create-view',
  templateUrl: './employee-create-view.component.html',
  styleUrls: ['./employee-create-view.component.css']
})
export class EmployeeCreateViewComponent {
  lastname: string = "";
  firstname: string = "";
  street: string = "";
  postcode: string = "";
  city: string = "";
  phone: string = "";

  constructor(
    private router: Router,
    private employeeService: EmployeeService,
    private app : AppComponent) {
    this.app.header = 1;
  }

  addEmployee() {
    if (this.lastname === null || this.firstname === null ||
        this.street === null || this.postcode === null||
        this.city === null || this.phone === null)
    {
      return;
    }

    if (this.lastname === "" || this.firstname === "" ||
      this.street === "" || this.postcode === "" ||
      this.city === "" || this.phone === "")
    {
      return;
    }

    this.employeeService.postEmployees(
      new Employee(0,
        this.lastname,
        this.firstname,
        this.street,
        this.postcode,
        this.city,
        this.phone
      ));
      this.router.navigate(['employee']);
    }

  cancelNewEmployeeView() {
      this.router.navigate(['employee']);
  }
}
