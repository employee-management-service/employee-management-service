import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HttpClientModule } from "@angular/common/http";
import { EmployeeViewComponent } from './component/employee-view/employee-view.component';
import { BearerTokenHolderService } from './service/BearerTokenHolder';
import { EmployeeService } from './service/employee.service';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Router, RouterModule } from "@angular/router";
import { QualificationViewComponent } from "./component/qualification-view/qualification-view.component";
import { EmployeeDetailViewComponent } from "./component/employee-detail-view/employee-detail-view.component";
import { CommonModule } from '@angular/common';
import {LoginViewComponent} from "./component/LoginView/login-view.component";
import {EmployeeCreateViewComponent} from "./component/employee-create-view/employee-create-view.component";

@NgModule({
  declarations: [
    AppComponent,
    LoginViewComponent,
    EmployeeViewComponent,
    QualificationViewComponent,
    EmployeeCreateViewComponent,
    EmployeeDetailViewComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule.forRoot([
      {path: '',component: LoginViewComponent},
      {path: 'employee', component:EmployeeViewComponent},
      {path: 'employee/new', component:EmployeeCreateViewComponent},
      {path: 'employee/detail', component:EmployeeDetailViewComponent},
      {path: 'qualification', component:QualificationViewComponent}
    ])
  ],
  providers: [BearerTokenHolderService, AppComponent,EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
