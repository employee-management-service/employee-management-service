import { Injectable, OnInit } from '@angular/core';
import { BearerToken } from '../model/BearerToken';
const url = "http://keycloak.szut.dev/auth/realms/szut/protocol/openid-connect/token";
@Injectable
({
  providedIn: 'root'
})
export class BearerTokenHolderService implements OnInit
{
  bearer: BearerToken = new BearerToken();
  ngOnInit(): void
  {}
}
