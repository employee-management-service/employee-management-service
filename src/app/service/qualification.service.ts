import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { Qualification } from '../model/Qualification';
import { BearerTokenHolderService } from '../service/BearerTokenHolder';
import {Employee} from "../model/Employee";

@Injectable
({
  providedIn: 'root'
})
export class QualificationService {


  constructor(
    private http: HttpClient,
    private bearerService : BearerTokenHolderService
  ) {}

  public getQualification() : Observable<Qualification[]>
  {
    return this.http.get<Qualification[]>('/backend/qualification', {
      headers: new HttpHeaders().append('Authorization', `Bearer ${this.bearerService.bearer.access_token}`).append('Content-Type', `application/json`)
    })
  }

  public postQualification(qualification : Qualification)
  {
    this.http.post<Qualification>('/backend/qualification', qualification, {
      headers : new HttpHeaders()
        .append('Authorization', `Bearer ${this.bearerService.bearer.access_token}`)
        .append('Content-Type', `application/json`)
    });
  }

  public findEmployeesByQualification(designation : string) : Observable<Employee[]>
  {
    return this.http.get<Employee[]>(`/backend/qualifications/${designation}/employees`,{
      headers: new HttpHeaders().append('Authorization', `Bearer ${this.bearerService.bearer.access_token}`)
        .append('Content-Type', `application/json`)
    })
  }


  public deleteQualification(id : number)
  {
    this.http.delete(`/backend/qualification/${id}`, {
      headers : new HttpHeaders()
        .append('Authorization', `Bearer ${this.bearerService.bearer.access_token}`)
        .append('Content-Type', `application/json`)
    });
  }
}
