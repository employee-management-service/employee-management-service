import { Component, OnInit } from '@angular/core';
import { AppComponent } from "../../app.component";
import { User } from "../../model/User";
import { Login } from "../../service/login";
import { Router } from "@angular/router";


@Component({
  selector: 'app-login-view',
  templateUrl: './login-view.component.html',
  styleUrls: ['./login-view.component.css']
})
export class LoginViewComponent implements OnInit {

  username: string = "";
  password: string = "";

  constructor(
    private router : Router,
    public app : AppComponent,
    private login : Login,
    ) {}

  ngOnInit(): void {}

  onSubmit(){
    const signInData = new User(this.username, this.password);
    if (this.login.tryLogin(signInData)){
      this.app.user = signInData;
      this.router.navigateByUrl("employee");
    }
  }
}
