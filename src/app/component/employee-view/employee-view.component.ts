import { Component, OnInit } from '@angular/core';
import {isEmpty, Observable, of} from 'rxjs';
import { Employee } from 'src/app/model/Employee';
import { EmployeeService } from 'src/app/service/employee.service';
import { Login} from "src/app/service/login";
import {Router} from "@angular/router";
import { QualificationService} from "../../service/qualification.service";
import {Qualification} from "../../model/Qualification";

@Component({
  selector: 'app-employee-view',
  templateUrl: './employee-view.component.html',
  styleUrls: ['./employee-view.component.css']
})
export class EmployeeViewComponent implements OnInit {
  employees: Observable<Employee[]> | undefined;
  qualifications: Observable<Qualification[]> | undefined;
  designation: string = "";

  constructor(
    private employeeService : EmployeeService,
    private loginService : Login,
    private qualifikationService : QualificationService,
    public router: Router
  ) {}

  loadEmployees() {
    this.employees = this.employeeService.getEmployees();
  }

  loadQualifications() {
    this.qualifications = this.qualifikationService.getQualification()
  }

  addNewQualification() {
    if (this.designation === null || this.designation === "")
    {
      return;
    }

    this.qualifikationService.postQualification(
      new Qualification(0, this.designation)
    )
  }

  deleteQualification(id : number): void {
    this.qualifikationService.deleteQualification(id)
    this.loadQualifications()
  }

  deleteEmployee(id:number): void {
    this.employeeService.deleteEmployee(id);
    this.loadEmployees();
  }

  addEmployee(){
    this.router.navigate(['employee/new']);
  }

  showEmployeesByDesignation(){
    //todo add pfad zu qualifications
  }

  editEmployee(id: number){
    this.router.navigate(['employee/update', id]);
  }

  goToQualifications(){
    this.router.navigateByUrl("qualification");
  }


  ngOnInit(): void {
    this.loadEmployees();
    this.loadQualifications();
  }

  onSubmitLogOutButton(){
    this.router.navigateByUrl("");
  }
}
